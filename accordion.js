import { LightningElement,track } from 'lwc';

export default class Basic extends LightningElement {
    @track changeOnClick='height:30px';
    @track showTable=false;
    @track changeIconOnClick='utility:chevronright';
    handleClick(){
        this.changeIconOnClick=(this.changeIconOnClick=='utility:chevronright'?'utility:chevrondown':'utility:chevronright')
        this.changeOnClick=(this.changeOnClick=='height:30px'?'background:yellow;height:30px;font-size:18px;':'height:30px');
        this.showTable=(this.showTable==true?false:true);
    }
}
